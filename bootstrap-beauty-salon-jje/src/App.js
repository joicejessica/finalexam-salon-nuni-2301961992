import { Container } from "react-bootstrap";
import AppFooter from "./components/AppFooter";
import AppNavbar from "./components/AppNavbar";
import Homepage from "./containers/Homepage";

function App() {
  return (
    <>
      <AppNavbar />
      <Container>
        <Homepage />
      </Container>
      <AppFooter />
    </>
  );
}

export default App;
