import create from 'zustand'
import { DUMMY_PRODUCTS } from './dummy'
import _includes from "lodash/includes"
import _orderBy from "lodash/orderBy"

const getProductsByOrder = (products, orderBy) => {
    switch (orderBy) {
        case "popularity":
            return _orderBy(products, ['review'], ['desc'])
        case "lowest-price":
            return _orderBy(products, ['price'], ['asc'])
        case "highest-price":
            return _orderBy(products, ['price'], ['desc'])
        case "latest-product":
            return _orderBy(products, ['isNew'], ['desc'])
        case "discount":
            return _orderBy(products, ['priceAfterDiscount'], ['desc'])
        default:
            return _orderBy(products, ['review'], ['desc']);
    }
}

export const useProductStore = create((set, get) => ({
    products: DUMMY_PRODUCTS,
    filteredProducts: DUMMY_PRODUCTS,
    query: '',
    orderBy: 'popularity',
    searchProducts: (query) => {
        set((state) => ({
            query,
            filteredProducts: state.products.filter(product => _includes(product.title.toLowerCase(), query.toLowerCase()))
        }))
    },
    orderProductsBy: (orderBy) => {
        set((state) => ({
            orderBy,
            filteredProducts: getProductsByOrder(state.filteredProducts, orderBy)
        }))
    },
    toggleFavorite: (id) => {
        set((state) => ({
            filteredProducts: state.filteredProducts.map(product => {
                if (product.id === id) return { ...product, isFavourite: !product.isFavourite }
                return product
            })
        }))
    },

}))