import { FiAnchor, FiGitlab, FiAward } from "react-icons/fi"

export const getIconComponentByName = (icon, size, color) => {
    switch (icon) {
        case "pirate":
            return <FiAnchor size={size} color={color} />
        case "catman":
            return <FiGitlab size={size} color={color} />
        case "champ":
            return <FiAward size={size} color={color} />
        default:
            return <FiAnchor size={size} color={color} />;
    }
}

export const formatMoney = (money) => {
    if (!money) return ""
    return new Intl.NumberFormat('id-ID',
        { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 } // diletakkan dalam object
    ).format(money);
}