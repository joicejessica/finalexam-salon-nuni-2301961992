export const COLOR = {
    MAIN_BACKGROUND_COLOR: "#38495D",
    PRIMARY_COLOR: "#E5493E",
    SECONDARY_COLOR: "#5787A3",
    TERTIARY_COLOR: "#FE9428",
    NEUTRAL_COLOR: "#B9B9B9",
    NEUTRAL_COLOR2: "#787878",
    DARK_COLOR: "#252B2B",
    LIGHT_COLOR: "#FFFFFF",
}

export const IMG = {
    HERO_1: './hero-1.jpg',
    HERO_2: './hero-2.jpg',
    HERO_3: './hero-3.jpg',
    PRODUCT_1: './product-1.jpg',
    PRODUCT_2: './product-2.jpg',
    PRODUCT_3: './product-3.jpg',
    PRODUCT_4: './product-4.jpg',
    PRODUCT_5: './product-5.jpg',
    PRODUCT_6: './product-6.jpg',
    PRODUCT_7: './product-7.jpg',
    PRODUCT_8: './product-8.jpg',
    PRODUCT_9: './product-9.jpg',
    PRODUCT_10: './product-10.jpg',
    PRODUCT_11: './product-11.jpg',
    PRODUCT_12: './product-12.jpg',
    PRODUCT_13: './product-13.jpg',
    PRODUCT_14: './product-14.jpg',
    PRODUCT_15: './product-15.jpg',
    PRODUCT_16: './product-16.jpg',
    PRODUCT_17: './product-17.jpg',
    PRODUCT_18: './product-18.jpg',
}

export const SORT_OPTIONS = [
    {
        label: 'Popularitas',
        value: 'popularity'
    },
    {
        label: 'Harga Terendah',
        value: 'lowest-price'
    },
    {
        label: 'Harga Tertinggi',
        value: 'highest-price'
    },
    {
        label: 'Produk Terbaru',
        value: 'latest-product'
    },
    {
        label: 'Harga Diskon',
        value: 'discount'
    },
]