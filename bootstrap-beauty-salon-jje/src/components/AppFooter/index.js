import React from 'react'
import { Col, Container, Row, Stack } from 'react-bootstrap'
import { FiFacebook, FiInstagram, FiTwitter, FiYoutube, FiLinkedin } from "react-icons/fi"
import AppFooterSubscriptionForm from './partials/AppFooterSubscriptionForm'

const AppFooter = () => {
    return (
        <footer className='bg-black py-5 text-white text'>
            <Container>
                <Row className='mb-4'>
                    <Col md={4}>
                        <h2 className='fs-1 fw-bold'>N U N I</h2>
                        <p className='mb-4 text-secondary'>Sebagai Pusat Fashion Online di Asia, kami menciptakan kemungkinan-kemungkinan gaya tanpa batas dengan cara memperluas jangkauan produk, mulai dari produk internasional hingga produk lokal dambaan. Kami menjadikan Anda sebagai pusatnya.</p>

                        <p className='fw-bold mb-0 text-secondary'> Layanan Pengaduan Konsumen </p>
                        <p className='mb-0 text-secondary'>E-mail: customer@id.nuni.com</p>
                        <p className='mb-4 text-secondary'>No. Tlp: 021-29490100 (Senin-Minggu, 09.00-18.00 WIB)</p>

                        <p className='fw-bold mb-0 text-secondary'>Direktorat Jenderal Perlindungan Konsumen dan Tertib Niaga Kementerian Perdagangan RI</p>
                        <p className='mb-4 text-secondary'>WhatsApp: +62 853 1111 1010</p>

                    </Col>
                    <Col md={2}>
                        <p className='fw-bold'>LAYANAN</p>
                        <ol className='list-unstyled text-secondary'>
                            <li><a href="/">Bantuan</a></li>
                            <li><a href="/">Cara Pengembalian</a></li>
                            <li><a href="/">Product Index</a></li>
                            <li><a href="/">Promo Partner Kami</a></li>
                            <li><a href="/">Konfirmasi Transfer</a></li>
                            <li><a href="/">Hubungi Kami</a></li>
                            <li><a href="/">Cara Berjualan</a></li>
                            <li><a href="/">Pengembalian Ongkir</a></li>
                            <li><a href="/">Status Order</a></li>
                        </ol>
                    </Col>
                    <Col md={2}>
                        <p className='fw-bold'>TENTANG KAMI</p>
                        <ol className='list-unstyled text-secondary'>
                            <li><a href="/">About Us</a></li>
                            <li><a href="/">Promosikan Brand Anda</a></li>
                            <li><a href="/">Pers / Media</a></li>
                            <li><a href="/">Karir</a></li>
                            <li><a href="/">Persyratan dan Ketentuan</a></li>
                            <li><a href="/">Kebijakan dan Privasi</a></li>
                            <li><a href="/">Responsible Disclosure</a></li>
                            <li><a href="/">Affiliate Marketing</a></li>
                            <li><a href="/">Influencer Program</a></li>
                        </ol>
                    </Col>
                    <Col md={4}>
                        <p className='fw-bold'>MEMBER BARU</p>
                        <p className='text-secondary mb-4'>Dapatkan <strong className='text-warning'>Rp 75.000 </strong>  VOUCHER (ditambah dengan berita fashion dan peluncuran brand terbaru) hanya dengan berlangganan newsletter kami.</p>
                        <AppFooterSubscriptionForm />

                    </Col>
                </Row>
                <Row className='mb-4'>
                    <Col>
                        <p className='fw-bold'>TEMUKAN KAMI</p>
                        <Stack gap={4} direction="horizontal" className='mb-4 social-media'>
                            <FiFacebook size={24} />
                            <FiInstagram size={24} />
                            <FiTwitter size={24} />
                            <FiYoutube size={24} />
                            <FiLinkedin size={24} />
                        </Stack>
                    </Col>
                    <Col />
                    <Col>
                        <p className='fw-bold'>DOWNLOAD APP KAMI SEKARANG</p>
                        <Stack gap={2} direction="horizontal" className='mb-4 text-secondary'>
                            <a href="/"><img className='play-store' src="./play-store.png" alt="google-play" /></a>
                            <a href="/"><img className='app-store' src="./app-store.png" alt="app store" /></a>
                        </Stack>
                    </Col>
                </Row>
                <Row>
                    <Col md={6} className='text-secondary'>
                        <p className='mb-0'>Anda punya pertanyaan ? Kami siap membantu</p>
                        <a href="/">Kontak</a> | <a href="/">Bantuan</a>
                    </Col>
                    <Col />
                    <Col md={6}>
                        <div className='text-secondary'>
                            <a href="/">Tentang NUNI</a> | <a href="/">Kebijakan Privasi</a> | <a href="/">Persyaratan dan Ketentuan</a>

                            <p className="ms-auto">© 2012-2022 NINU</p>
                        </div>
                    </Col>
                </Row>
            </Container>
        </footer>
    )
}

export default AppFooter