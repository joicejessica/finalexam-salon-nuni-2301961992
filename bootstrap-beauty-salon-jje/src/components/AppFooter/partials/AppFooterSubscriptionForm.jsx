import React from 'react'
import { Stack } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form'

const AppFooterSubscriptionForm = () => {
    return (
        <Form>
            <Form.Group controlId="formBasicEmail" className='mb-2'>
                <Form.Label className='text-secondary'>Alamat Email Kamu</Form.Label>
                <Form.Control type="email" placeholder="someone@example.com" />
            </Form.Group>

            <Stack direction='horizontal' gap={2} className="mb-2">
                <Button variant="outline-light" type="submit" className='w-100 border-radius-0' >
                    WANITA
                </Button>
                <Button variant="outline-light" type="submit" className='w-100 border-radius-0' >
                    PRIA
                </Button>
            </Stack>
            <p className='text-secondary'>Dengan mendaftar, Anda menyetujui persyaratan dalam Kebijakan Privasi kami</p>
        </Form>
    )
}

export default AppFooterSubscriptionForm