import React from 'react'
import PropTypes from 'prop-types'
import { AiFillHeart, AiOutlineHeart } from "react-icons/ai"
import { COLOR } from '../../constant'

const FavouriteButton = ({ isFavourite, onClickHandler }) => {

    return (
        <button className="no-style ms-auto" onClick={onClickHandler}>
            {isFavourite && <AiFillHeart className='ms-auto' color={COLOR.PRIMARY_COLOR} size={24} />}
            {!isFavourite && <AiOutlineHeart className='ms-auto' size={24} />}
        </button>
    )
}

FavouriteButton.propTypes = {
    isFavourite: PropTypes.bool.isRequired,
    onClickHandler: PropTypes.func.isRequired,
}

export default FavouriteButton