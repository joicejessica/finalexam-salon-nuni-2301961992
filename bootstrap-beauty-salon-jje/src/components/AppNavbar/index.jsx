import MainNavbar from "./partials/MainNavbar";
import SecondaryNavbar from "./partials/SecondaryNavbar";

const AppNavbar = () => {
  return (
   <div className="mb-4">
    <MainNavbar/>
    <SecondaryNavbar/>
   </div>
  )
}


export default AppNavbar;