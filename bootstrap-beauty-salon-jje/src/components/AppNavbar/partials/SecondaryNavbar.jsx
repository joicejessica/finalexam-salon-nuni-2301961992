import React from 'react'
import { Container } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { FiBox, FiPhoneCall, FiTag, FiTruck } from "react-icons/fi"

const SecondaryNavbar = () => {
    return (
        <Container>
            <Nav
                activeKey="/home"
                onSelect={(selectedKey) => alert(`selected ${selectedKey}`)}
                className="secondary-navbar"
            >
                <Nav.Item>
                    <Nav.Link href="/"><FiBox />PRODUK ORIGINAL & TERJAMIN</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-1"><FiTag />RIBUAN FASHION BRAND</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-2"><FiTruck />GRATIS PENGEMBALIAN</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link eventKey="link-2"><FiPhoneCall />PERTANYAAN</Nav.Link>
                </Nav.Item>
            </Nav>
        </Container>
    )
}

export default SecondaryNavbar