import React, { useState } from 'react'
import Container from 'react-bootstrap/Container';
import Form from 'react-bootstrap/Form';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Offcanvas from 'react-bootstrap/Offcanvas';
import Badge from 'react-bootstrap/Badge';
import { FiSearch, FiUser, FiHeart, FiShoppingBag } from "react-icons/fi"
import { useProductStore } from '../../../store';

const MainNavbar = () => {

    const searchProducts = useProductStore((state) => state.searchProducts)
    const filteredProducts = useProductStore((state) => state.filteredProducts)
    const [text, setText] = useState('')

    const searchTextHandler = (e) => {
        setText(e.target.value)
    }

    const searchButtonHandler = (e) => {
        e.preventDefault()
        console.log('text', text)
        searchProducts(text)
    }

    const userButtonHandler = () => {
        console.log('search');
    }

    const favButtonHandler = () => {
        console.log('faforite');
    }

    const shoppingButtonHandler = () => {
        console.log('shopping');
    }

    const countFavorite = (products) => {
        return products.filter(product => product.isFavourite === true).length
    }


    return (
        <>
            {['xl'].map((expand) => (
                <Navbar key={expand} fixed='top' bg='dark' expand={expand} className="main-navbar">
                    <Container>
                        <Navbar.Brand href="#" className='fw-bold fs-2'>N U N I</Navbar.Brand>
                        <Navbar.Toggle className="text-white" aria-controls={`offcanvasNavbar-expand-${expand}`} />
                        <Navbar.Offcanvas
                            id={`offcanvasNavbar-expand-${expand}`}
                            aria-labelledby={`offcanvasNavbarLabel-expand-${expand}`}
                            placement="end"
                        >
                            <Offcanvas.Header closeButton>
                                <Offcanvas.Title id={`offcanvasNavbarLabel-expand-${expand}`}>
                                    Offcanvas
                                </Offcanvas.Title>
                            </Offcanvas.Header>
                            <Offcanvas.Body>
                                <Nav className="justify-content-start flex-grow-1 pe-3">
                                    <Nav.Link href="#action1">SKIN CARE</Nav.Link>
                                    <Nav.Link href="#action2">MAKE UP</Nav.Link>
                                    <Nav.Link href="#action2">HAIR CARE</Nav.Link>
                                    <Nav.Link href="#action2">FRAGRANCE</Nav.Link>
                                    <Nav.Link href="#action2">MENS CARE</Nav.Link>
                                </Nav>
                                <Form className="d-flex gap-2">
                                    <div className='form-search'>
                                        <Form.Control
                                            type="search"
                                            placeholder="Search"
                                            className="form-control form-search__form-control"
                                            aria-label="Search"
                                            onChange={searchTextHandler}
                                        />
                                        <button className='no-style form-search__button' onClick={searchButtonHandler}>
                                            <FiSearch size={24} />
                                        </button>
                                    </div>
                                    <button className='no-style' onClick={userButtonHandler}>
                                        <FiUser size={24} color="white" />
                                    </button>
                                    <button className='main-navbar__favorite' onClick={favButtonHandler}>
                                        <Badge bg="danger">{countFavorite(filteredProducts)}</Badge>
                                        <FiHeart size={24} color="white" />
                                    </button>
                                    <button className='no-style' onClick={shoppingButtonHandler}>
                                        <FiShoppingBag size={24} color="white" />
                                    </button>
                                </Form>
                            </Offcanvas.Body>
                        </Navbar.Offcanvas>
                    </Container>
                </Navbar>
            ))}
        </>
    )
}

export default MainNavbar