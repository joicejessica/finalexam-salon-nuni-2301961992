import React from 'react'
import Breadcrumb from 'react-bootstrap/Breadcrumb';

const AppBreadCrumbs = () => {
    return (
        <Breadcrumb>
            <Breadcrumb.Item href="#">Home</Breadcrumb.Item>
        </Breadcrumb>
    )
}

export default AppBreadCrumbs