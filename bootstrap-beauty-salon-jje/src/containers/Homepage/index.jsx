import React from 'react'
import ScrollButton from '../../components/ScrollButton'
import HomepageCards from './partials/HomepageCards'
import HomepageFilter from './partials/HomepageFilter'
import HomepageHero from './partials/HomepageHero'

const Homepage = () => {
    return (
        <>
            <HomepageHero />
            <HomepageFilter />
            <HomepageCards />
            <ScrollButton />
        </>
    )
}

export default Homepage