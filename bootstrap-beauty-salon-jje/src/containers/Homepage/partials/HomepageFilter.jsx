import React from 'react'
import { Col, Row, Stack } from 'react-bootstrap'
import { useProductStore } from '../../../store'
import HomepageSelect from './HomepageSelect'

const HomepageFilter = () => {
    const query = useProductStore(state => state.query)
    const filteredProducts = useProductStore(state => state.filteredProducts)
    const totalProducts = filteredProducts.length;
    return (
        <>
            <Row className='mb-4'>
                <Col xl={6}>
                    {query && <p>Pencarian untuk : <strong>"{query}"</strong></p>}
                </Col>
                <Col xl={6}>
                    <Stack direction='horizontal' gap={3} c>
                        <span className='ms-auto'>{totalProducts} Produk</span> |
                        <strong className='fs-lg'>ATUR BERDASARKAN</strong>
                        <HomepageSelect />
                    </Stack>
                </Col>
            </Row>

        </>
    )
}

export default HomepageFilter