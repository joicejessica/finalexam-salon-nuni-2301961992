import React from 'react'
import Carousel from 'react-bootstrap/Carousel';
import { IMG } from '../../../constant';

const HomepageHero = () => {
    return (
        <Carousel className='mb-4'>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={IMG.HERO_1}
                    alt="First slide"
                />
                <Carousel.Caption>
                    <h2>Tampil Cantik Dengan Promo Menarik</h2>
                    <p className='font-secondary'>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={IMG.HERO_2}
                    alt="Second slide"
                />

                <Carousel.Caption>
                    <h2 className='font-secondary'>Saatnya Restock Produk Kecantikan Kamu</h2>
                    <p className='font-secondary'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                    className="d-block w-100"
                    src={IMG.HERO_3}
                    alt="Third slide"
                />

                <Carousel.Caption>
                    <h2 className='font-secondary'>Promo Menarik Awal Tahun</h2>
                    <p className='font-secondary'>
                        Praesent commodo cursus magna, vel scelerisque nisl consectetur.
                    </p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    )
}

export default HomepageHero