import React, { useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import ReactPaginate from 'react-paginate';
import { formatMoney } from '../../../helper'
import { useProductStore } from '../../../store'
import HomepageCard from './HomepageCard'

const Items = ({ currentItems }) => {
  return (
    <Row sm={1} md={2} lg={3} xl={4}>
      {
        currentItems?.map(product => <Col key={product.id}>
          <HomepageCard
            {...product}
            price={formatMoney(product.price)}
            priceAfterDiscount={formatMoney(product.priceAfterDiscount)}
          />
        </Col>)
      }
    </Row>
  );
}



const HomepageCards = props => {
  const items = useProductStore(state => state.filteredProducts)
  const itemsPerPage = 8;
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0);

  // Simulate fetching items from another resources.
  // (This could be items from props; or items loaded in a local state
  // from an API endpoint with useEffect and useState)
  const endOffset = itemOffset + itemsPerPage;
  console.log(`Loading items from ${itemOffset} to ${endOffset}`);
  const currentItems = items.slice(itemOffset, endOffset);
  const pageCount = Math.ceil(items.length / itemsPerPage);

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % items.length;
    console.log(
      `User requested page number ${event.selected}, which is offset ${newOffset}`
    );
    setItemOffset(newOffset);
  };

  return (
    <div className="mb-5">
      <Items currentItems={currentItems} />
      <div className="d-flex">
        <ReactPaginate
          breakLabel="..."
          nextLabel=">"
          onPageChange={handlePageClick}
          pageRangeDisplayed={5}
          pageCount={pageCount}
          previousLabel="<"
          renderOnZeroPageCount={null}
          className="pagination ms-auto"
          pageClassName="page-item"
          pageLinkClassName="page-link"
          activeClassName="active"
          previousClassName="page-prev"
          nextClassName="page-next"
          previousLinkClassName="page-link"
          nextLinkClassName="page-link"
        />
      </div>
    </div>
  )
}

export default HomepageCards