import React from 'react'
import { Badge, Stack } from 'react-bootstrap';
import Card from 'react-bootstrap/Card';
import PropTypes from "prop-types"
import FavouriteButton from '../../../components/FavouriteButton';
import { useProductStore } from '../../../store';

const renderBenefits = (benefits) => {
    if (!benefits) return null

    return (
        <div className='homepage-card__discounts'>
            {
                benefits.map((benefit, index) => <Badge key={`benefit-${index}`} bg="secondary">
                    {benefit}
                </Badge>)
            }
        </div>
    )
}

const renderPriceByDiscount = (discount, price, priceAfterDiscount) => {
    if (discount) {
        return (
            <>
                <Stack direction='horizontal' gap={2}>
                    <Badge bg="danger" className='mr-2'>{discount}% OFF</Badge>
                    <Card.Title className='mb-0'>
                        <s className='fs-6 text-secondary'>{price}</s>
                    </Card.Title>
                </Stack>
                <Card.Title className='text-danger fw-bold'>
                    SEKARANG <span className='fs-da'>{priceAfterDiscount}</span>
                </Card.Title>
            </>
        )
    }

    return <Card.Title className='mb-0'>{price}</Card.Title>
}

const renderNewProduct = (isNew) => {
    if (!isNew) return null

    return (
        <Badge bg="dark" className="homepage-card__new">NEW</Badge>
    )
}


const HomepageCard = (props) => {
    const { id, img, benefits, discount, price, priceAfterDiscount, title, desc, isFreeDelivery, isFavourite, isNew } = props;
    const toggleFavorite = useProductStore(state => state.toggleFavorite)
    const onClickFavoriteButtonHandler = (id) => {
        toggleFavorite(id)
    }

    return (
        <Card className='mb-4 homepage-card p-3'>
            <div className='homepage-card__image-container'>
                <Card.Img className='homepage-card__image' variant="top" src={img} />
                {renderBenefits(benefits)}
                {renderNewProduct(isNew)}
            </div>
            <Card.Body>
                {renderPriceByDiscount(discount, price, priceAfterDiscount)}
                <hr />
                <Stack direction='horizontal' className='mb-2'>
                    <Card.Title className='mb-0'>{title}</Card.Title>
                    <FavouriteButton isFavourite={isFavourite} onClickHandler={() => onClickFavoriteButtonHandler(id)} />
                </Stack>
                <Card.Text className='text-small font-secondary'>
                    <small>{desc}</small>
                </Card.Text>
                {isFreeDelivery && <Badge pill bg="secondary">
                    Gratis Ongkir
                </Badge>}
            </Card.Body>

        </Card>
    )
}

HomepageCard.propTypes = {
    id: PropTypes.number.isRequired,
    review: PropTypes.number.isRequired,
    title: PropTypes.string.isRequired,
    desc: PropTypes.string.isRequired,
    isNew: PropTypes.bool.isRequired,
    isFavourite: PropTypes.bool.isRequired,
    isFreeDelivery: PropTypes.bool.isRequired,
    price: PropTypes.string.isRequired,
    discount: PropTypes.number,
    priceAfterDiscount: PropTypes.string,
    benefits: PropTypes.arrayOf(PropTypes.string),
    img: PropTypes.string.isRequired
}

export default HomepageCard