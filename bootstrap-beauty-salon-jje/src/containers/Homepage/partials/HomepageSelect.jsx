import React from 'react'
import Form from 'react-bootstrap/Form';
import { SORT_OPTIONS } from '../../../constant';
import { useProductStore } from '../../../store';

const HomepageSelect = () => {
  const orderBy = useProductStore(state => state.orderBy)
  const orderProductsBy = useProductStore(state => state.orderProductsBy)

  const onChangeHandler = (e) => {
    orderProductsBy(e.target.value)
  }

  return (
    <Form.Select
      aria-label="Select"
      style={{ width: 200 }}
      onChange={onChangeHandler}
      value={orderBy}
    >
      {
        SORT_OPTIONS.map((option, index) => <option
          key={`sort-option-${index}`}
          value={option.value}>{option.label}</option>)
      }

    </Form.Select>
  )
}

export default HomepageSelect